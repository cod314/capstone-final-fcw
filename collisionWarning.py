import label_image
import cv2

cap = cv2.VideoCapture('Media/FinalVideo2.mov')
ret, image = cap.read()

# used to set up video writer
image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)
gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
imHeight, imWidth = gray_image.shape
out = cv2.VideoWriter('test.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (imWidth, imHeight))

buffer = ['x', 'x', 'x', 'x', 'x']
BLUE = [255,140,0]
count = 1
colorStatus = 'x'

# 3 element buffer, majority rule
# iterate through video
while cv2.waitKey(1):

    ret, image = cap.read()  # read frame
    image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)  # resize just for fun
    gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)  # converting to gray image just to get height & width

    imHeight, imWidth = gray_image.shape  # get height & width of image

    # want to process every 10 frames
    if count % 10 == 0:

        #ROI's for 11/14/18 video aka train5.mp4
        # redROI = image[(imHeight * 7 / 10): (imHeight * 9 / 10),
        #          imWidth * 3 / 10: imWidth * 3 / 5]  # crop image to redROI
        # yellowROI = image[(imHeight * 3/5): (imHeight * 3 / 4),
        #             imWidth * 35 / 100: imWidth * 60 / 100]  # crop image to yellowROI
        redROI = image[(imHeight * 65 / 100): (imHeight * 80 / 100),
                 imWidth * 3 / 10: imWidth * 3 / 5]  # crop image to redROI
        yellowROI = image[(imHeight * 3 / 5): (imHeight * 3 / 4),
                    imWidth * 35 / 100: imWidth * 60 / 100]  # crop image to yellowROI
        cv2.imwrite("image.jpg", redROI)  # save image (want to save so we can pass path to label_image)

        # top_k, results, labels = label_image.label("image.jpg")
        top_k, results, labels = label_image.red_label("image.jpg") # using red graph

        # template = "{} (score={:0.5f})"  # needed for the for loop from label_image.py

        # get the category of most and least likely (car or not)
        negLabel = labels[0]
        posLabel = labels[1]

        # get the percentage result of most and least likely
        positiveResult = results[1]
        negativeResult = results[0]

        # CHECKING RED CONDITIONS FIRST
        # using 70% arbitrarily
        if positiveResult > negativeResult and positiveResult > 0.60:
            cv2.circle(image, (imWidth * 4 / 5, imHeight / 4), 40, (0, 0, 255), -1)
            buffer[4] = buffer[3]
            buffer[3] = buffer[2]
            buffer[2] = buffer[1]
            buffer[1] = buffer[0]
            buffer[0] = 'r'
            colorStatus = 'r'
            # print ("RED", positiveResult)
        # elif negativeResult > positiveResult and negativeResult > 0.70:  # REDROI was negative, so check YELLOWROI
        else:
            cv2.imwrite("image.jpg", yellowROI)
            # top_k, results, labels = label_image.label("image.jpg")
            top_k, results, labels = label_image.yellow_label("image.jpg") #using yellow graph
            positiveResult = results[1]
            negativeResult = results[0]
            if positiveResult > negativeResult and positiveResult > 0.70:  # found a car in YELLOWROI
                cv2.circle(image, (imWidth * 4 / 5, imHeight / 4), 40, (0, 255, 255), -1)
                # cv2.copyMakeBorder(image, 10, 10, 10, 10, cv2.BORDER_CONSTANT, value=BLUE)
                buffer[4] = buffer[3]
                buffer[3] = buffer[2]
                buffer[2] = buffer[1]
                buffer[1] = buffer[0]
                buffer[0] = 'y'
                # print("updated yellow buffer")
                colorStatus = 'y'
                # print ("YELLOW", positiveResult)
            else: #didn't successfully pass red OR yellow conditions
                # print("in buffer")
                redCount = 0
                yellcount = 0
                # go through buffer to collect 'votes'
                for elem in buffer:
                    if elem == 'y':
                        yellcount = yellcount + 1
                    elif elem == 'r':
                        redCount = redCount + 1
                if redCount >= 3: #red vote wins
                    cv2.circle(image, (imWidth * 4 / 5, imHeight / 4), 40, (0, 0, 255), -1)
                    colorStatus = 'r'
                elif yellcount >= 3: #yellow vote wins
                    cv2.circle(image, (imWidth * 4 / 5, imHeight / 4), 40, (0, 255, 255), -1)
                    colorStatus = 'y'
                elif redCount + yellcount >= 3: #majority is some combo of red/yellow
                    for elem in buffer: #find first elem that is a color, draw the color, break
                        if elem == 'y':
                            cv2.circle(image, (imWidth * 4 / 5, imHeight / 4), 40, (0, 255, 255), -1)
                            colorStatus = 'y'
                            break
                        elif elem == 'r':
                            cv2.circle(image, (imWidth * 4 / 5, imHeight / 4), 40, (0, 0, 255), -1)
                            colorStatus = 'r'
                            break
                else: #might need to change
                    colorStatus = 'x'
                buffer[4] = buffer[3]
                buffer[3] = buffer[2]
                buffer[2] = buffer[1]
                buffer[1] = buffer[0]
                buffer[0] = 'x' # need to update buffer so that it doesn't count as red/yellow but missed

        # for i in top_k:
        #     print(template.format(labels[i], results[i]))
        # print results


    else:
        if colorStatus == 'y':
            cv2.circle(image, (imWidth * 4 / 5, imHeight / 4), 40, (0, 255, 255), -1)
        elif colorStatus == 'r':
            cv2.circle(image, (imWidth * 4 / 5, imHeight / 4), 40, (0, 0, 255), -1)


    cv2.imshow('object detect', image)
    # cv2.imshow('REDROI', redROI)
    # cv2.imshow('YELLOW', yellowROI)
    out.write(image)
    count = count + 1

cap.release()
out.release()
cv2.destroyAllWindows()