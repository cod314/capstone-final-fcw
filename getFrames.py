import subprocess
import cv2
# import label_image

# vidcap = cv2.VideoCapture('trainingVids/train8.mp4')
vidcap = cv2.VideoCapture('Media/demo2.mp4')

success,image = vidcap.read()
count = 22000
while cv2.waitKey(1):

    ret, image = vidcap.read()  # read frame5
    if count % 20 == 0:
        image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)
        gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

        imHeight, imWidth = gray_image.shape

        redROI = image[(imHeight * 3 / 4): (imHeight * 9 / 10),
                 imWidth * 2 / 5: imWidth * 3 / 4]  # crop image to redROI
        yellowROI = image[(imHeight * 13/20): (imHeight * 3 / 4),
                    imWidth * 1 / 2: imWidth * 3 / 5]  # crop image to yellowROI

        cv2.imshow('REDROI', redROI)
        cv2.imshow('YELLOW', yellowROI)
        while True:
            k = cv2.waitKey(1)
            if k == ord('r'): #found positive red frame
                cv2.imwrite("redPos/rframe%d.jpg" % count, redROI)
                break
            elif k == ord('y'): #found positive yellow frame
                cv2.imwrite("yellPos/yframe%d.jpg" % count, yellowROI)
                break
            elif k == ord('n'): #negative so save in repsective location
                cv2.imwrite("redNeg/rframe%d.jpg" % count, redROI)
                cv2.imwrite("yellNeg/yframe%d.jpg" % count, yellowROI)
                break
            elif k == ord('x'):  # don't like any images
                break


    # if count % 20 == 0:
      # cv2.imwrite("22vid%d.jpg" % count, cropped)     # save frame as JPEG file
      # cv2.imshow("frame%d.jpg" % count, cropped

      # success,image = vidcap.read()
      # print('Read a new frame: ', success)
    count += 1


vidcap.release()
# out.release()
cv2.destroyAllWindows()