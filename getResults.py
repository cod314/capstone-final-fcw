import label_image
import cv2

cap = cv2.VideoCapture('trainingVids/miniTrain.mp4')
ret, image = cap.read()

# used to set up video writer
image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)
gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
imHeight, imWidth = gray_image.shape
# out = cv2.VideoWriter('test.avi', cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 10, (imWidth, imHeight))

count = 1
colorStatus = 'x'

# 3 element buffer, majority rule
# iterate through video
while cv2.waitKey(1):

    ret, image = cap.read()  # read frame
    image = cv2.resize(image, (0, 0), fx=0.5, fy=0.5)  # resize just for fun
    gray_image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)  # converting to gray image just to get height & width

    imHeight, imWidth = gray_image.shape  # get height & width of image

    # want to process every 10 frames
    if count % 60 == 0:

        redROI = image[(imHeight * 65 / 100): (imHeight * 80 / 100),
                 imWidth * 3 / 10: imWidth * 3 / 5]  # crop image to redROI
        yellowROI = image[(imHeight * 3 / 5): (imHeight * 3 / 4),
                    imWidth * 35 / 100: imWidth * 60 / 100]  # crop image to yellowROI
        cv2.imwrite("image.jpg", redROI)  # save image (want to save so we can pass path to label_image)

        # top_k, results, labels = label_image.label("image.jpg")
        top_k, results, labels = label_image.red_label("image.jpg") # using red graph

        # template = "{} (score={:0.5f})"  # needed for the for loop from label_image.py

        # get the category of most and least likely (car or not)
        negLabel = labels[0]
        posLabel = labels[1]

        # get the percentage result of most and least likely
        positiveResult = results[1]
        negativeResult = results[0]

        # CHECKING RED CONDITIONS FIRST
        if positiveResult > negativeResult and positiveResult > 0.60:
            cv2.circle(image, (imWidth * 4 / 5, imHeight / 4), 40, (0, 0, 255), -1)
        else:
            cv2.imwrite("image.jpg", yellowROI)

            top_k, results, labels = label_image.yellow_label("image.jpg") #using yellow graph
            positiveResult = results[1]
            negativeResult = results[0]
            if positiveResult > negativeResult and positiveResult > 0.70:  # found a car in YELLOWROI
                cv2.circle(image, (imWidth * 4 / 5, imHeight / 4), 40, (0, 255, 255), -1)


        cv2.imshow('object detect', image)
        cv2.imshow('REDROI', redROI)
        cv2.imshow('YELLOW', yellowROI)

        while True:
            k = cv2.waitKey(1)
            if k == ord('x'):  # don't like any images
                break

    # out.write(image)
    count = count + 1

cap.release()
# out.release()
cv2.destroyAllWindows()