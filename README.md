# Overview

This repository is a modified version of the TensorFlow for Poets repository. I was able to modify the files and train my own neural nets in order to determine the proximity of a car in front of someone driving on a highway. Through the use of two neural nets, one for a red region of interest and yellow region of interest, I was able to create a Forward Collision Warning system implemented in Python with the help of TensorFlow. The ending accuracy of the models was about 92%.

